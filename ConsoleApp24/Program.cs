﻿using System;
using System.IO;

namespace ConsoleApp24 {
    class Program
    {
        public static int choice;
        static void Main(string[]args)
        {
                Console.WriteLine("Оберiть опцiю:");
                Console.WriteLine("1. Вивести кiлькiсть слiв у текстi \"Lorem ipsum\"");
                Console.WriteLine("2. Виконати математичну операцiю множення");
                Console.WriteLine("3. Вийти");

                if (int.TryParse(Console.ReadLine(), out choice))
                {
                    switch (choice)
                    {
                        case 1:
                            CountWordsInLoremIpsum();
                            break;
                        case 2:
                            int result = PerformMathOperation();
                        Console.WriteLine($"Результат: {result}");
                            break;
                        case 3:
                            Environment.Exit(0);
                            break;
                        default:
                            Console.WriteLine("Невiрний вибір. Спробуйте ще раз.");
                            break;
                    }
                }
                else
                {
                    Console.WriteLine("Невiрний ввід. Будь ласка, введiть число.");
                }
        }

        static void CountWordsInLoremIpsum()
        {
            string loremIpsumText = File.ReadAllText("LoremIpsum.txt");
            string[] words = loremIpsumText.Split(new char[] { ' ', '\n', '\r', '\t' });
            Console.Write("Введiть кiлькiсть слiв для зчитування: ");
            int numWords = int.Parse(Console.ReadLine());
            for(int i = 0; i < numWords; i++) {
                Console.Write($"{words[i]} ");
            }
        }

        static int PerformMathOperation()
        {
            Console.WriteLine("Введiть перше число:");
            int firsNumber =int.Parse(Console.ReadLine());
            Console.WriteLine("Введiть друге число:");
            int secondNumber =int.Parse(Console.ReadLine());
            return firsNumber * secondNumber;
        }
    }
}


